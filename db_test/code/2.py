import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from data.models import Article
from django.db import connection
from timeit import timeit

def test_1():
    all = Article.objects.all()
    return len(all)

def test_2():
    all = Article.objects.all()
    return all.count()

t1 = timeit(test_1, number=100)
t2 = timeit(test_2, number=100)

print "Len: {}".format(t1)
print "Cnt: {}".format(t2)

