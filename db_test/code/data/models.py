from django.db import models


class Article(models.Model):
    name = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)
    body = models.TextField()
    visits = models.IntegerField()

