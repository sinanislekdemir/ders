import os
from random import randint
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from data.models import Article
from django.db import connection

f = open('data.txt', 'r')
text = f.read()
f.close()

text = text.replace("\n", " ")

pos = 0

while pos <= len(text) - 100:
    name = text[pos:pos+100]
    title = text[pos:pos+200]
    body = text[pos:pos+1000]
    visits = randint(0, 100)
    Article.objects.create(
            name=name,
            title=title,
            slug='',
            body=body,
            visits=visits)
    pos += 1000
    print "created {}".format(name)
