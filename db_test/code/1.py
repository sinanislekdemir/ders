import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from data.models import Article
from django.db import connection

# sample 1
all = Article.objects.all()
a = len(all)
# a = all.count()
all = Article.objects.all().values('title')
if all:
    print connection.queries

