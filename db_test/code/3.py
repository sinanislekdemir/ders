import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from data.models import Article
from django.db import connection
from django.db.transaction import atomic
from django.db.models import F
from timeit import timeit

def test_1():
    all = Article.objects.all()
    for record in all:
        record.visits += 1
        record.save()

@atomic
def test_2():
    all = Article.objects.all()
    for record in all:
        record.visits += 1
        record.save()

def test_2_wrapper():
    test_2()

def test_3():
    Article.objects.update(visits=F('visits')+1)

print "Working"
t1 = timeit(test_1, number=1)
print "Normal: {}".format(t1)
t2 = timeit(test_2_wrapper, number=1)
print "Atomic: {}".format(t2)
t3 = timeit(test_3, number=1)
print "F     : {}".format(t3)
