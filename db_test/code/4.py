import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from data.models import Article
from django.db import connection
from django.db.models import Avg, Max, Min
from timeit import timeit

def test_1():
    all = Article.objects.all()
    total = 0
    max = all.first().visits
    min = all.first().visits
    for rec in all:
        total += rec.visits
        if rec.visits > max:
            max = rec.visits
        if rec.visits < min:
            min = rec.visits
    avg = total / all.count()
    return avg, max, min

def test_2():
    data = Article.objects.aggregate(Avg('visits'), Max('visits'),
            Min('visits'))
    return data['visits__avg'], data['visits__max'], data['visits__min']

print "Working"
t1 = timeit(test_1, number=100)
t2 = timeit(test_2, number=100)

print "Normal   : {}".format(t1)
print "Aggregate: {}".format(t2)

