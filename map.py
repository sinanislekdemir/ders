import random
from timeit import timeit

print "Generating numbers"
alist = random.sample(range(1, 1000000), 10000)


# do = lambda i: i ** 2
def do(i):
    return i ** 2


def imperative():
    final = map(do, alist)
    return final


def declarative():
    final = []
    for x in alist:
        final.append(do(x))
    return final


def declarative_list():
    final = [do(x) for x in alist]
    return final


print "Imperative test 100 ex"
t1 = timeit(imperative, number=1000)
print "took {} secs".format(t1)

print "Declarative test 100 ex"
t2 = timeit(declarative, number=1000)
print "took {} secs".format(t2)

print "Declarative List Compr. 100 ex"
t3 = timeit(declarative_list, number=1000)
print "took {} secs".format(t3)
