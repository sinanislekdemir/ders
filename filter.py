import random
from timeit import timeit

print "Generating numbers"
alist = random.sample(range(1, 1000000), 10000)


def my_filter(n):
    return n % 2 == 0


def imperative():
    final = filter(my_filter, alist)
    return final


def declarative():
    final = []
    for x in alist:
        if my_filter(x):
            final.append(x)
    return final


def declarative_list():
    final = [x for x in alist if my_filter(x)]
    return final


print "Imperative test 100 ex"
t1 = timeit(imperative, number=1000)
print "took {} secs".format(t1)

print "Declarative test 100 ex"
t2 = timeit(declarative, number=1000)
print "took {} secs".format(t2)

print "Declarative List Compr. 100 ex"
t3 = timeit(declarative_list, number=1000)
print "took {} secs".format(t3)
