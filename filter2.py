import csv
import io
from timeit import timeit

sample_data = []
f = open('data.csv')
data = f.read()
f.close()
data = csv.DictReader(io.StringIO(unicode(data, 'utf-8')))

for r in data:
    sample_data.append(r)


def my_filter(row):
    return row['status'] == 'booked'


def imperative():
    final = filter(my_filter, sample_data)
    return final


def declarative():
    final = []
    for row in sample_data:
        if my_filter(row):
            final.append(row)
    return final


def declarative_list():
    final = [row for row in sample_data if my_filter(row)]
    return final


print "Imperative test 100 ex"
t1 = timeit(imperative, number=1000)
print "took {} secs".format(t1)

print "Declarative test 100 ex"
t2 = timeit(declarative, number=1000)
print "took {} secs".format(t2)

print "Declarative List Compr. 100 ex"
t3 = timeit(declarative_list, number=1000)
print "took {} secs".format(t3)
