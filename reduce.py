import random
from timeit import timeit

print "Generating numbers"
alist = random.sample(range(1, 1000000), 10000)


def biggest(a, b):
    return a if a > b else b


def imperative():
    final = reduce(biggest, alist)
    return final


def declarative_reduce(func, iterable, start=None):
    it = iter(iterable)
    if start is None:
        start = next(it)
    acc_val = start
    for x in iterable:
        acc_val = func(acc_val, x)
    return acc_val


def declarative():
    final = declarative_reduce(biggest, alist)
    return final


print "Imperative test 100 ex"
t1 = timeit(imperative, number=1000)
print "took {} secs".format(t1)

print "Declarative test 100 ex"
t2 = timeit(declarative, number=1000)
print "took {} secs".format(t2)
